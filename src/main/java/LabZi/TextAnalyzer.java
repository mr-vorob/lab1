package LabZi;
import static LabZi.MainApp.alphabet;

public class TextAnalyzer {

    private int lengthCode = 0;
    public void analyze(String code) {
        for (char element : code.toCharArray()) {
            if (element == ' ') continue;
            ++lengthCode;
        }
    }

    public double[] analyzeText(String text) {
        double[] lengthStone = new double[alphabet.length()];
        for (int i = 0; i < alphabet.length(); i++) {
            for (int j = 0; j < text.length(); j++) {
                if (alphabet.charAt(i) == text.charAt(j)) {
                    ++lengthStone[i];
                }
            }
            lengthStone[i] = (lengthStone[i] / lengthCode) * 100;
        }
        return lengthStone;
    }
}
