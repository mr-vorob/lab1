package LabZi;

import java.io.*;

public class MainApp {
    public static String alphabet = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
    public static void main(String... args) throws Exception {
        //test
        String key = "котя";

        Encryptor encryptor = new Encryptor();
        encryptor.encryptor(key);
        String code = encryptor.getCode().toString();

        TextAnalyzer textAnalyzer = new TextAnalyzer();
        textAnalyzer.analyze(encryptor.getText());
        double[] lengthStone;
        lengthStone=textAnalyzer.analyzeText(encryptor.getText());


        TextAnalyzer textAnalyzer1Code = new TextAnalyzer();
        textAnalyzer1Code.analyze(encryptor.getText());
        double[] lengthCodes;
        lengthCodes=textAnalyzer1Code.analyzeText(code);


        StringBuffer uncodeAlf = new StringBuffer(alphabet.length());
        for (int i = 0; i < alphabet.length(); i++) {
            for (int j = 0; j < alphabet.length(); j++) {
                if (lengthStone[i] == 0) break;
                if (lengthStone[i] == lengthCodes[j]) {
                    uncodeAlf.append(alphabet.charAt(j));
                    System.out.print("Буква " + alphabet.charAt(i) + "=" + lengthStone[i] + "  " + alphabet.charAt(j) + "=" + lengthCodes[j] + "\n");
                }
            }
        }

        StringBuffer uncode = new StringBuffer(code.length());
        for (int i = 0; i < code.length(); i++) {
            try {
                uncode.append(alphabet.charAt(uncodeAlf.indexOf(String.valueOf(code.charAt(i)))));
            } catch (StringIndexOutOfBoundsException e) {
                uncode.append(" ");
                continue;
            }
        }
        System.out.print(uncode + "\n");
    }
}

