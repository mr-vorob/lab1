package LabZi;

import java.io.*;

import static LabZi.MainApp.alphabet;

public class Encryptor {

    private StringBuilder alphabetCode = new StringBuilder(alphabet);
    private String text;
    private StringBuffer code;

    public void encryptor(String key){

        for (int j = 0; j < key.length(); j++) {
            alphabetCode.deleteCharAt(alphabetCode.indexOf(String.valueOf(key.charAt(j))));
        }
        alphabetCode.insert(0, key);

        StringBuilder sb = new StringBuilder();
        File file = new File("file.txt");
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(file));
            String text;

            while ((text = reader.readLine()) != null) {
                sb.append(text);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
            }
        }

        text = sb.toString();
        text = text.toLowerCase();

        code = new StringBuffer(text.length());
        for (int i = 0; i < text.length(); i++) {
            try {
                code.append(alphabetCode.charAt(alphabet.indexOf(text.charAt(i))));
            } catch (StringIndexOutOfBoundsException e) {
                code.append(" ");
                continue;
            }

        }

        File codeFile = new File("code.txt");
        try {
            if (!codeFile.exists()) {
                codeFile.createNewFile();
            }
            PrintWriter out = new PrintWriter(codeFile.getAbsoluteFile());
            try {
                out.print(code);
            } finally {
                out.close();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public StringBuffer getCode() {
        return code;
    }

    public String getText() {
        return text;
    }
}
